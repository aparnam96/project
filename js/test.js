$(document).ready(function()
{
	$("#banner").hide(1000);
	//to set max date to today.
	var x = new Date().toJSON().slice(0,10);
    document.getElementById("dob").max = x;

    (function(){
      createCaptcha();
    })();
	function randomNumberGenerator(min, max)
	{
		let random_number = Math.random() * (max-min) + min;
		return Math.floor(random_number);
	}

	function createCaptcha()
	{
		let op = ['+','-','*','/'];
		let x = randomNumberGenerator(50,100);
		let y = randomNumberGenerator(0,50);
		let z = x.toString();
		z = z + op[randomNumberGenerator(0,4)];
		z = z + y.toString();
		$("#captcha").text(z);
		let ans = eval (z);
		if(Number.isInteger(ans) === false)
		{
		   createCaptcha();
		}
		
	}
	$("#captcha-answer").on('change',function()
	{
		let $this = $(this);
		console.log($("#captcha"));
	    let captcha = $("#captcha").text();
	    let result = eval(captcha);
	    if($this.val() == result)
	    {
	      removeErrorMessage($this);
	    }
	    else
	    {
	      errorMessage($this,"Captcha result is not valid");
	    }
	});

	$("#refresh-btn").on('click',function(){
       createCaptcha();
	});
	$("#fname,#mname,#sname").on('change',function()
	{    
		let $this = $(this);
	    let pattern = /^[A-Za-z][A-Za-z]*$|^$/;
	    let msg = "Name is not valid";
	    check($this, pattern, msg);
	});
	$("#user-password").on('change',function()
	{
		let $this = $(this);
		let pattern = /^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z])[A-Za-z0-9]{6,20}$|^$/;
		let msg = "Incorrect Syntax of password";
		check($this, pattern, msg);
	});
	$("#email").on('change',function()
	{
	   let $this = $(this);
       let pattern = /[a-z0-9!#$%&'*+/=?^_`"{|}~-]+(\.[a-z0-9!#$%&'*+/=?^_"`{|}~-]+)*@([a-z0-9]([a-z0-9-]*[a-z0-9])?\.)+[a-z0-9]([a-z0-9-]*[a-z0-9])?/;
       let msg = "Invalid email Address";
       check($this, pattern, msg);
	});
	$("#phn-number, #alt-number").on("change",function(){
		let $this = $(this);
		let pattern = /^[0-9]{10}$|^$/;
		let msg = "Phone Number is not valid";
		check($this, pattern ,msg);
	});
	$("#submit").on("click", function()
	{
        var valid = "true";

	});
    function check($this,pattern,msg)
    { 
	   if(pattern.test($this.val()) === false)
	    {
	    	errorMessage($this,msg)
	    }
	    else
	    {
            removeErrorMessage($this)
	    }
	}
	function errorMessage($this,msg)
	{
		let id = "#"+$this.attr('id')+"-error";
        $(id).css("color","red");
	    $(id).text(msg); 
	}
	function removeErrorMessage($this)
	{
       let id = "#"+$this.attr('id')+"-error";
       $(id).text("");
	}
});
/*var x = function(){
  console.log("I m called from inside");
};
var y = function(callback)
{
  console.log("do something");
  callback();
};
y(x);



$(document).ready(function(){
	$("#b").on('click',function(){
     //$("#a").appendTo($(this));
     $(this).clone().text("hello").appendTo($(this));
	});
});*/