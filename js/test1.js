$(document).ready(function()
{
	var x = new Date().toJSON().slice(0,10);
    document.getElementById("dob").max = x;

    (function(){
      createCaptcha();
    })();
	function randomNumberGenerator(min, max)
	{
		let random_number = Math.random() * (max-min) + min;
		return Math.floor(random_number);
	}

	function createCaptcha()
	{
		let op = ['+','-','*','/'];
		let x = randomNumberGenerator(50,100);
		let y = randomNumberGenerator(0,50);
		let z = x.toString();
		z = z + op[randomNumberGenerator(0,4)];
		z = z + y.toString();
		$("#captcha").text(z);
		let ans = eval (z);
		if(Number.isInteger(ans) === false)
		{
		   createCaptcha();
		}
	}
	$("#captcha-answer").on('change',function()
	{
		let $this = $(this);
	    let captcha = $("#captcha").text();
	    let result = eval(captcha);
	    if($this.val() == result)
	    {
	      removeErrorMessage($this);
	    }
	    else
	    {
	      errorMessage($this,"Captcha result is not valid");
	    }
	});

	$("#refresh-btn").on('click',function(){
       createCaptcha();
	});

    var field = function ($this){
	          var pattern;
	          var msg;
              var name =
              {
             	pattern :/^[A-Za-z][A-Za-z]*$|^$/,
             	msg:"Name is not valid"
              };
              var email =
              {
                pattern:/[a-z0-9!#$%&'*+/=?^_`"{|}~-]+(\.[a-z0-9!#$%&'*+/=?^_"`{|}~-]+)*@([a-z0-9]([a-z0-9-]*[a-z0-9])?\.)+[a-z0-9]([a-z0-9-]*[a-z0-9])?/,
                msg : "Email address is not valid"
              };
              var password =
              {
		        pattern : /^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z])[A-Za-z0-9]{6,20}$|^$/,
              	msg : "Password is not valid" 
              };
              var phone = 
              {
              	pattern : /^[0-9]{10}$|^$/,
              	msg : "Phone number is not valid"
              };
              if($this === "fname" || $this === "mname" || $this === "sname")
              {
              	return name;
              }
              else if($this === "email")
              {
              	return email;
              }
              else if($this === "user-password")
              {
              	return password;
              }
              else if($this === "phn-number"||$this === "alt-number")
              {
              	return phone;
              }
            };

$("#fname, #mname, #sname, #user-password, #email, #phn-number, #alt-number").on("change",function()
{
   var $this = $(this);	
   var get = field($this.attr('id'));
   check($this, get.pattern, get.msg);
}); 
$("#submit").on("click",function(){
  var valid = true;
  var required_msg = "Field is required" ;
  var arr = ["#fname","#mname", "#sname", "#email", "#user-password", "#phn-number", "#alt-number"];
  arr.forEach(function(item)
  {
  	$this = $(item);
  	if(checkFormValue($this) === false)
  	{
  		valid = false;
  	}
  });
  debugger;
  
  $gender = $("#gender");
  if($("#male").is(checked) === false && $("#female").is(checked) === false && $("#other").is(checked) === false) 
  {
  	valid = false;
    errorMessage($gender, "Please select the gender");
  }
  else
  {
  	removeErrorMessage($gender);
  }
  return valid;
});
function checkFormValue($this)
{
	var valid = true;
	var required_msg = "Field is required" ;
	if($this.val() === "")
	{
		valid = false;
		errorMessage($this ,required_msg);
	}
	else
	{
		if(accessErrorMessage($this)!== "")
		{
			valid = false;
		}
	}
	return valid;
}
function check($this,pattern,msg)
{ 
	if(pattern.test($this.val()) === false)
	{
	    errorMessage($this,msg);
	}
	else
	{
        removeErrorMessage($this);
	}
}
function errorMessage($this,msg)
{
	let id = "#"+$this.attr('id')+"-error";
    $(id).css("color","red");
	$(id).text(msg); 
}
function accessErrorMessage($this)
{
	let id = "#"+$this.attr('id')+"-error";
	return($(id).text());
}
function removeErrorMessage($this)
{
    let id = "#"+$this.attr('id')+"-error";
    $(id).text("");
}           
});