(function(){
   myFunction();
})();
function randomNumber(min ,max)
{
	let random_number = Math.random() * (max-min) + min;
    return Math.floor(random_number);

}

function myFunction()
{
   let op = ['+','-','*','/'];
   var x = randomNumber(50,100);
   var y = randomNumber(0,50);
   var z = x.toString();
   z = z + op[randomNumber(0,4)];
   z = z + y.toString();
   let ans = eval (z);
   console.log(ans);
   console.log(Number.isInteger(ans));
   document.getElementById('Add-button-captcha').innerText = z;
   if(Number.isInteger(ans) === false)
   {
    	myFunction();
   }
   
}
document.getElementById('first-name').addEventListener('input',check);
document.getElementById('middle-name').addEventListener('input',check);
document.getElementById('surname').addEventListener('input',check);
document.getElementById('email').addEventListener('change',check);
document.getElementById('phn-number-country-code').addEventListener('change',checkInputHaveValues);
document.getElementById('alt-number-country-code').addEventListener('change',checkInputHaveValues);
document.getElementById('dob').addEventListener('change',checkInputHaveValues);
document.getElementById('personal-interest').addEventListener('change',checkInputHaveValues);
document.getElementById('current-city').addEventListener('change',check);
document.getElementById('current-state').addEventListener('change',check);
document.getElementById('current-country').addEventListener('change',check);
document.getElementById('permanent-city').addEventListener('change',check);
document.getElementById('permanent-state').addEventListener('change',check);
document.getElementById('permanent-country').addEventListener('change',check);
document.getElementById('permanent-postal-code').addEventListener('change',postalCode);
document.getElementById('current-postal-code').addEventListener('change',postalCode);
document.getElementById('user-password').addEventListener('change',check);
document.getElementById('confirm-password').addEventListener('change',checkEqual);
document.getElementById('phn-number').addEventListener('change',checkLength);
document.getElementById('alt-number').addEventListener('change',checkLength);
document.getElementById('current-equal-permanent-address').addEventListener('click',copyAddress);
document.getElementById('captcha-answer').addEventListener("change",calculate);

function calculate()
{
	let captcha = document.getElementById('Add-button-captcha').innerText;
    var result = eval(captcha);
    if(this.value == result)
    {
      removeErrorMessage(this.id);
    }
    else{
    	console.log(this.value);
    	message("Invalid captcha", this.id);
    }

}
function checkInputHaveValues()
{
	if(this.value !=="")
	{
       removeErrorMessage(this.id);
	}
}
function check()
{
	let id = this.id;
	let value = this.value;
	let pattern;
	let msg;
	if((id === "first-name" )||(id === "middle-name")||(id === "surname"))
	{
	  pattern = /^[A-Za-z]+([.'-]*)?$|^$/;
	  msg =	"Invalid name";
	}
	if(id === "email")
	{
	   pattern = /^$|^([a-z.0-9+""]+)@([a-z]+\.[a-z]{2,6})$/;
	   msg = "Invalid Email format";
	}
	if(id === "user-password")
	{
		pattern = /^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{6,20}$/;
		msg = "Invalid sytax of password";
	}
	if((id === "current-city")||(id === "current-state")||(id ==="current-country")||(id ==="permanent-city")||(id ==="permanent-country")
		||(id ==="permanent-state"))
	{
		pattern = /^[A-Za-z.']+$|^$/;
		msg ="Enter a valid address";
	}
	checkInput(id,value,pattern,msg);
}
function postalCode()
{

	let pattern = /current/g;
	let a;
	if(pattern.test(this.id))
	{
	   a = document.getElementById('current-country').value ;
    }else
    {
       a = document.getElementById('permanent-country').value;
    }
    if( a !== "" )
    {
    	removeErrorMessage(this.id);
	let postal = {
		India:/^\d{6}$/,
        USA:/^\d{5}([\-]?\d{4})?$/,
        UK:/^[A-Z]{1,2}[0-9R][0-9A-Z]?\\s*[0-9][A-Z-[CIKMOV]]{2}/,
        SouthAfrica:/^\\d{4}$/,
        Germany:/^\\d{5}$/
	}
	let propertyName = a;
	pattern = postal[propertyName];
	console.log(pattern);
	if(pattern.test(this.value) === false)
	{
		message("Invalid postal code",this.id);

	}else{
       removeErrorMessage(this.id);
	}
    }else{
    	message("Country name is required" ,this.id);
    }
}
function checkInput(id,value,pattern,msg)
{
	if(pattern.test(value) === false)
	{
		message(msg,id);
	}
	else
	{
        removeErrorMessage(id);
	}   
}
function checkEqual()
{   
	let id =this.id;
	if(this.value !== document.getElementById('user-password').value)
	{
		let msg = "Password does not match";
		message(msg ,id);
	}
	else
	{
        removeErrorMessage(id);
	}

}
function validKey(event)
{
	var num = event.key;	
    if(/[0-9]/.test(num) === false)
	{
		event.preventDefault();
	}	
}
function checkLength()
{
	
	if(document.getElementById(this.id+"-country-code").value === "")
	{
		message("Country Code is required",this.id);
	}
    else{
    	removeErrorMessage(this.id);
		if(this.value.length <10)
		{
			message("Your phone number must contain 10 digits", this.id);
		}
		else{
			removeErrorMessage(this.id);
		}
    }

}
function copyAddress()
{
	if(this.checked)
	{
		var current_address = [
			            "address",
	                    "city",
	                    "state",
	                    "country",
	                    "postal-code"];
	     var correct = 0;
	     var s = 0;              
	    for(let i=0; i<5; i++)
	    {  
            let index ="current-"+current_address[i];
            console.log(index);
            if(document.getElementById(index).value === "")
            {
            	correct = 1;
            	message("Field is required",index);
            }
            else
            {
            	removeErrorMessage(index);
            }
	    }
	   
	    if(correct === 0)
	    {
	      for(let i=0; i<5; i++)
	      {
	      	 let index_current ="current-"+current_address[i];
	      	 console.log(index_current);
	      	 let index_permanent ="permanent-"+current_address[i];
             console.log(index_permanent);
	      	 document.getElementById(index_permanent).value = document.getElementById(index_current).value;
	      }	
	    }else{
	    	document.getElementById(this.id).checked = false;
	    }             

	}
}
function checkGengerIsSelected()
{
	let male  = document.getElementById('male');
	let female = document.getElementById('female');
	let other = document.getElementById('other');
	if(male.checked === true ||female.checked === true ||other.checked === true)
	{
		removeErrorMessage('gender');
	}

	
}
function message(msg, div_id)
{
	div_id = div_id+"-error";
	document.getElementById(div_id).innerHTML = "";
	var span = document.createElement('span');
    span.innerHTML = msg;
    span.style.color = 'red';
	document.getElementById(div_id).append(span);
	return false;
}
function removeErrorMessage(div_id)
{
	div_id = div_id+"-error";
	document.getElementById(div_id).innerHTML = "";
}
function checkEmpty()
{
	let array_div = ["first-name","surname","email","phn-number","dob","user-password","confirm-password",
	              "current-address","current-city","current-state","current-country","current-postal-code",
	              "permanent-address","permanent-city","permanent-state","permanent-country","permanent-postal-code",
	                "personal-interest","captcha-answer"]
	let check_array =[];
	let length = array_div.length;
	let msg = "Field is required";
	for(let i=0; i<length ;i++)
	{
		if(document.getElementById(array_div[i]).value === "")
		{
			check_array.push(1);
           message(msg, array_div[i]);
             
        }
		else
		{
			if(document.getElementById(array_div[i]+"-error").innerHTML === "Field is required" )
			{
               removeErrorMessage(array_div[i]);
            }
		}
	}
	if(document.getElementById('male').checked === false)
	{
		if(document.getElementById('female').checked === false)
		{
			if(document.getElementById('other').checked === false )
			{
				check_array.push(1);
				message("Select Gender ", "gender");
			}
		}
	}
	else
	{
		removeErrorMessage("gender");
	}       
    for (let i=0;i<check_array.length;i++)
    {
    	if(check_array[i] === 1)
    	{
    		return false;
    	}
    }
   return true;
}
function validateForm()
{
  let x = checkEmpty();
  if(x === false)
  {
  	return false;
  }
  else 
  	return true;
}